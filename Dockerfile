# tomamos como imagen docker principal la de tomcat que incluye una jre (maquina virtual de java)
FROM tomcat:8-jre8 

# declaramos variables de entorno con las ubicaciones de las carpetas que nos interesan
ENV CATALINA_HOME /usr/local/tomcat
ENV PATH $CATALINA_HOME/bin:$PATH

# definimos cual va a ser nuestro directorio de trabajo, o sea, los comandos que tiremos se ejecutaran
# sobre esta ruta /usr/local/tomcat

WORKDIR $CATALINA_HOME

# agregamos a tomcat nuestro war de ejemplo en esta ruta /usr/local/tomcat/webapps/sample.war

ADD sample.war $CATALINA_HOME/webapps/

# exponemos nuestro tomcat en el puerto 8080

EXPOSE 8080

# ejecutamos el servidor de tomcat

CMD ["catalina.sh", "run"]


